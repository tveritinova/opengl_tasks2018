#include "MazeApplication.hpp"

MeshPtr MazeApplication::makeWall(int flag) {

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    // 1
    vertices.push_back(glm::vec3(0.0, 0.0, 0.0));
    vertices.push_back(glm::vec3(0.0, 0.0, 1.0));
    if (flag == 1) {
        vertices.push_back(glm::vec3(1.0, 0.0, 0.0));
    }
    if (flag == -1) {
        vertices.push_back(glm::vec3(0.0, -1.0, 0.0));
    }

    if (flag == 1) {
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    } 

    if (flag == -1) {
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    }

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    // 2
    vertices.push_back(glm::vec3(0.0, 0.0, 1.0));
    if (flag == 1) {
        vertices.push_back(glm::vec3(1.0, 0.0, 1.0));
        vertices.push_back(glm::vec3(1.0, 0.0, 0.0));
    }
    if (flag == -1) {
        vertices.push_back(glm::vec3(0.0, -1.0, 1.0));
        vertices.push_back(glm::vec3(0.0, -1.0, 0.0));
    }

    if (flag == 1) {
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    } 

    if (flag == -1) {
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    }

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


void MazeApplication::makeScene() {
    MeshPtr mesh;

    _floor = makeGroundPlane((float) _size / 2, 2, glm::vec3(0.0, 0.0, 1.0));
    _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

    _ceiling = makeGroundPlane((float) _size / 2, 2, glm::vec3(0.0, 0.0, -1.0));
    _ceiling->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 1.0f)));

    for (std::vector<int> vec: _walls) {

        mesh = makeWall(vec[2]);
        mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), 
            glm::vec3(
                float(vec[1]) - float(_size)/2.0f, 
                -float(vec[0]) + float(_size)/2.0f, 
                0.0f)));
        _walls_mesh.push_back(mesh);
    }      

    Application::makeScene();

    _cameraMover = std::make_shared<FreeCameraMover>();
    _cameraMover->walls = _walls;
    _cameraMover->shift = float(_size)/2.0f;
    _cameraMover->maze_start = 
        glm::vec3(
            _start[1] - float(_size)/2.0f + 0.5f, 
            -_start[0] + float(_size)/2.0f + 2.0f, 
            0.0f);
    
    _shader = std::make_shared<ShaderProgram>(
        "task1/495Tveritinova/495TveritinovaData/shaderNormal.vert",
        "task1/495Tveritinova/495TveritinovaData/shader.frag");
}

void MazeApplication::draw() {

    Application::draw();

    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Устанавливаем шейдер
    _shader->use();

    //Устанавливаем общие юниформ-переменные
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
    _floor->draw();

    if (_is_show_ceiling) {
        _shader->setMat4Uniform("modelMatrix", _ceiling->modelMatrix());
        _ceiling->draw();
    }

    for (int i=0; i < _walls_mesh.size(); i++) {
        _shader->setMat4Uniform("modelMatrix", _walls_mesh[i]->modelMatrix());
        _walls_mesh[i]->draw();
    }
}