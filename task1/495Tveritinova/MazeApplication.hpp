#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>


class MazeApplication : public Application {
public:
    MeshPtr _floor;
    MeshPtr _ceiling;
    std::vector<MeshPtr> _walls_mesh;
    int _size;
    std::vector<std::vector<int>> _walls;
    ShaderProgramPtr _shader;
    bool _is_show_ceiling;
    std::vector<int> _start;

    MazeApplication(bool is_show_ceiling, int size, 
        std::vector<std::vector<int>> walls, std::vector<int> start):
        _is_show_ceiling(is_show_ceiling), _size(size), _walls(walls), _start(start) {}

    MeshPtr makeWall(int flag); 
    void makeScene() override;
    void draw() override;
};