#include <iostream>
#include <fstream>
#include <set>
#include <numeric>
#include <algorithm>
#include <vector>
#include "MazeApplication.hpp"

void read_walls(std::ifstream& maze_struct, 
    std::vector<std::vector<int>>& walls, std::vector<int>& start, int size) {
    
    char flag;
    for (int i=0; i < size*2+1; i++) {
        for (int j=0; j < size*2+1; j++) {
            maze_struct >> flag;

            if (flag == '-') {
                walls.push_back({i/2, j/2, 1});
            }

            if (flag == '|') {
                walls.push_back({i/2, j/2, -1});
            }

            if (flag == '*') {
                start = {i/2, j/2};
            }
        }
    }
}

int main() {

    int size;
    bool is_show_ceiling;

    std::ifstream maze_struct("task1/495Tveritinova/495TveritinovaData/structure", std::ofstream::in);

    maze_struct >> is_show_ceiling;
    maze_struct >> size;

    std::vector<std::vector<int>> walls;
    std::vector<int> start;

    read_walls(maze_struct, walls, start, size);

    MazeApplication app(is_show_ceiling, size, walls, start);
    app.start();
    return 0;
}